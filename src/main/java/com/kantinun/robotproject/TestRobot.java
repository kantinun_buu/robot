/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantinun.robotproject;

/**
 *
 * @author EAK
 */
public class TestRobot {
    public static void main(String[] args) {
        Robot robot = new Robot(0, 0, 3, 3, 10);
        System.out.println(robot);
        robot.walk('N');
        System.out.println(robot);
        robot.walk('S', 2);
        System.out.println(robot);
        robot.walk();
        System.out.println(robot);
        robot.walk('E');
        System.out.println(robot);
        robot.walk(2);
        System.out.println(robot);
    }
}
